﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace updater_cli
{
    class Json
    {
        public static AppInfo GetAppInfo(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<AppInfo>(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n\t" + ex.Message + "\n");
                AppInfo info = new AppInfo();
                return info;
            }
        }

       public static void GenerateAppInfoToFile(AppInfo app)
        {
            Console.WriteLine("\n\t" + "Creating AppInfo.json..." + "\n");
            try
            {
                JsonSerializer serializer = new JsonSerializer();
                using (StreamWriter DataFile = new StreamWriter("AppInfo.json", false))
                using (JsonWriter writer = new JsonTextWriter(DataFile))
                {
                    writer.Formatting = Formatting.Indented;
                    serializer.Serialize(writer, app);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n\t" + ex.Message + "\n");
            }
        }
    }
}
