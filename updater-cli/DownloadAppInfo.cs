﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace updater_cli
{
    class DownloadAppInfo
    {
       public static void GetFileFromURL(string DownloadURL, ref string result)
        {
            Console.WriteLine("\n\tLooking for new version of app..." + "\n");
            try
            {
                WebClient client = new WebClient();
                result = client.DownloadString(DownloadURL);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n\t" + ex.Message + "\n");
            }
        }
    }
}
