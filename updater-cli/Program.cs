﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace updater_cli
{
    class Program
    {
        private static string checksum;

        static void Main(string[] args)
        {
            Console.Title = "Updater";
            //You must fill upp fields of class AppInfo before run (If u want generate AppInfo)

            //AppInfo appInfo = new AppInfo();
            //appInfo.AppName = "SNM4AD";
            //appInfo.Author = "Adrian Kastrau";
            //appInfo.Version = "1.0";
            //appInfo.File = "SNM4ADGUIWFORMS.exe";
            //appInfo.DownloadURL = "https://bitbucket.org/akastrau/snm4ad_gui_wforms/downloads/SNM4ADGUIWFORMS.exe";
            //appInfo.ChangelogText = "Stable release";
            //appInfo.Comments = "Published";
            //appInfo.RequireAdditionalComponents = "yes";
            //appInfo.AdditionalComponents = "libs.zip";
            //appInfo.AdditionalComponentsUrl = "https://bitbucket.org/akastrau/snm4ad_gui_wforms/downloads/components.zip";

            //Console.WriteLine("\n\tCalculating checksum...\n");
            //try
            //{
            //    string checksum = Calculate_Checksum.GetHashFromFile(appInfo.File, Calculate_Checksum.SHA512);
            //    appInfo.Hash = checksum;
            //}
            //catch (Exception ex)
            //{

            //    Console.WriteLine(ex.Message);
            //}

            //Json.GenerateAppInfoToFile(appInfo);
            string AppInfo = "";
            DownloadAppInfo.GetFileFromURL("https://bitbucket.org/akastrau/snm4ad_gui_wforms/downloads/AppInfo.json", ref AppInfo);
            AppInfo info = Json.GetAppInfo(AppInfo);

            try
            {
                checksum = Calculate_Checksum.GetHashFromFile(info.File, Calculate_Checksum.SHA512);
            }
            catch (Exception)
            {
                Downloader.GetAppFromURL(info);
                Console.Write("\n\tDone!\n\n\tPress any key... ");
                Console.ReadKey();
                return;
            }
            
            if (checksum != info.Hash )
            {
                Downloader.GetAppFromURL(info);
                if (info.ChangelogText != null)
                {
                    Console.WriteLine("\n\tChangelog:\n\t{0}\n", info.ChangelogText);
                }
                Console.Write("\n\tDone!\n\n\tPress any key... ");
                Console.ReadKey();
            }
            else
            {
                Console.Write("\n\tNo new updates are available\n\n\tPress any key... ");
            }
            Console.ReadKey();
        }
    }
}
