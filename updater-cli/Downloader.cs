﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace updater_cli
{
    public static class ZipArchiveExtensions
    {
        public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite)
        {
            if (!overwrite)
            {
                archive.ExtractToDirectory(destinationDirectoryName);
                return;
            }
            foreach (ZipArchiveEntry file in archive.Entries)
            {
                string completeFileName = Path.Combine(destinationDirectoryName, file.FullName);
                if (file.Name == "")
                {// Assuming Empty for Directory
                    Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                    continue;
                }
                file.ExtractToFile(completeFileName, true);
            }
        }
    }

    class Downloader
    {
        public static void GetAppFromURL(AppInfo app)
        {
            try
            {
                WebClient client = new WebClient();
                Console.WriteLine("\n\tDownloading app from repo...\n");

                if (app.RedirectTo != null)
                {
                    client.DownloadFile(app.RedirectTo, app.File);
                    if (!VerifyDownload(app.File, app.Hash)) throw new Exception ("Corrupted file! Exitting...");
                }
                else
                {
                    client.DownloadFile(app.DownloadURL, app.File);
                    if(!VerifyDownload(app.File, app.Hash)) throw new Exception("Corrupted file! Exitting...");
                }
                if (app.RequireAdditionalComponents == "yes")
                {
                    Console.WriteLine("\n\tDowloading components..." + "\n");
                    client.DownloadFile(app.AdditionalComponentsUrl, app.AdditionalComponents);
                    Console.WriteLine("\n\tExtracting..." + "\n");
                    ZipArchive zip = ZipFile.OpenRead(app.AdditionalComponents);
                    ZipArchiveExtensions.ExtractToDirectory(zip, Environment.CurrentDirectory, true);
                    zip.Dispose();
                    File.Delete(app.AdditionalComponents);
                }
            }
            catch (Exception ex)
            {
                if (File.Exists(app.File))
                {
                    File.Delete(app.File);
                }
                Console.WriteLine("\n\t" + ex.Message + "\n");
                Console.Write("\n\tCritical error occurred :(\n\tPress any key to exit... ");
                Console.ReadKey();
                Environment.Exit(1);
            }
        }

        public static bool VerifyDownload(string FileName, string Checksum)
        {
            if (Calculate_Checksum.GetHashFromFile(FileName, Calculate_Checksum.SHA512) != Checksum) return false;
            return true;
        }
    }
}
