﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater_gui
{
    public class AppInfo
    {
        public string AppName { get;  set; }
        public string File { get;  set; }
        public string DownloadURL { get;  set; }
        public string RedirectTo { get;  set; }
        public string Author { get;  set; }
        public string Version { get;  set; }
        public string Hash { get;  set; }
        public string ChangelogText { get;  set; }
        public string Comments { get;  set; }
        public string RequireAdditionalComponents { get;  set; }
        public string AdditionalComponents { get; set; }
        public string AdditionalComponentsUrl { get; set; }
    }
}
