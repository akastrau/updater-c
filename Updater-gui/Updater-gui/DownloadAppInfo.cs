﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Updater_gui
{
    class DownloadAppInfo
    {
       public static void GetFileFromURL(string DownloadURL, ref string result)
        {
            MainWindow window = MainLauncher.GetMainWindow();
            window.ChangeStatusText("Checking for updates...");
            
            try
            {
                WebClient client = new WebClient();
                result = client.DownloadString(DownloadURL);
            }
            catch (Exception ex)
            {
                window.ShowErrorMessage(ex.Message);
            }
        }
    }
}
